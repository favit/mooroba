exports.generateReturnMessage = function(requesterId, requestUrl, requestMessage, results, err){
    var message = require("../models/messageModel.js");
    
    message.messageModel.requesterId = requesterId;
    message.messageModel.requestUrl = requestUrl;
    message.messageModel.requestMessage = requestMessage
                    
    if(err) {
      message.messageModel.responseMessage = err;
      message.messageModel.errorCode = "500";
      message.messageModel.result="FAILURE";     
    } else {
      message.messageModel.responseMessage = results;
      message.messageModel.errorCode = "200";
      message.messageModel.result="SUCCESS";
    }
    return message;
};

exports.getCurrentDateTime = function(){
    var now = new Date();
    var jsonDate = now.toJSON();
    var then = new Date(jsonDate);

    return then;
};