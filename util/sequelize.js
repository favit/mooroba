var config = require("../com/config.js");
var Sequelize = require('sequelize');

var sequelize = new Sequelize(config.mysql.database,
                              config.mysql.user,
                              config.mysql.password,
                              {
                                host: config.mysql.hostName,
                                dialect: 'mysql',

                                pool: {
                                    max: 5,
                                    min: 0,
                                    idle: 10000
                                }
                              });

var user = sequelize.define('User', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
        },
    social_id: {
                type: Sequelize.INTEGER,
                unique: true,
                validate: {
                    notNull: true
                }
            },
    email: {
            type: Sequelize.STRING(50),
            unique: true,
            validate: {
                isEmail: true
            }
        },
    birthday: {
            type: Sequelize.DATEONLY,
            validate: {
                notNull: true
            }
        },
    gender: {
            type: Sequelize.STRING(1),
            defaultValue: 'M',
        },
    country: {
            type: Sequelize.INTEGER
        },
    lat: {
        type: Sequelize.DOUBLE,
        defaultValue: 37.564174
    },
    lng: {
        type: Sequelize.DOUBLE,
        defaultValue: 126.997576
    },
    points: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    grade: {
        type: Sequelize.STRING(2),
        defaultValue: 'A'
    },
    start_date: {
        type: Sequelize.DATEONLY
    },
    end_date: {
        type: Sequelize.DATEONLY
    }},
    {
      timestamps: true,
      createdAt: 'create_date',
      updatedAt: 'update_date',

      tableName: 'User'
    });

var poll = sequelize.define('Poll', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
        },
    user_id: {
                type: Sequelize.INTEGER,
                validate: {
                    notNull: true
                }
            },
    bg_img: {
            type: Sequelize.STRING(100),
        },
    content: {
            type: Sequelize.TEXT('tiny'),
            validate: {
                notNull: true
            }
        },
    option: {
            type: Sequelize.STRING(5),
            validate: {
                notNull: true
            }
        },
    status: {
            type: Sequelize.STRING(2),
            defaultValue: 'P'
        }
    },
    {
      timestamps: true,
      createdAt: 'create_date',
      updatedAt: 'update_date',

      tableName: 'Poll'
    });

var selection = sequelize.define('Selection', {
    user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            validate: {
                notNull: true
            }
        },
    poll_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            validate: {
                notNull: true
            }
        },
    option_id: {
            type: Sequelize.INTEGER,
            validate: {
                notNull: true
            }
        }
    },
    {
      timestamps: true,
      createdAt: 'create_date',
      updatedAt: false,

      tableName: 'Selection'
    });

var option = sequelize.define('Option', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
        },
    poll_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            validate: {
                notNull: true
            }
        },
    text: {
            type: Sequelize.STRING
        },
    img: {
            type: Sequelize.STRING
        }
    },
    {
      timestamps: false,

      tableName: 'Option'
    });

var point = sequelize.define('Point', {
     user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            validate: {
                notNull: true
            }
        },
    payment_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            validate: {
                notNull: true
            }
        },
    point: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
    status: {
            type: Sequelize.STRING(10),
            defaultValue: 'BUY'
        }
    method: {
            type: Sequelize.STRING(15),
            defaultValue: 'naver'
        }
    },
    {
      timestamps: true,
      createdAt: 'create_date',
      updatedAt: 'update_date',

      tableName: 'Point'
    });

var payment = sequelize.define('Payment', {
     id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
    user_id: {
            type: Sequelize.INTEGER,
            validate: {
                notNull: true
            }
        },
    amount: {
            type: Sequelize.INTEGER,
            defaultValue: 0
        },
    method: {
            type: Sequelize.STRING(15),
            defaultValue: 'naver'
        }
    },
    {
      timestamps: true,
      createdAt: 'create_date',
      updatedAt: 'update_date',

      tableName: 'Payment'
    });

//module.exports.getUser = function(device_id, callback){
//    User.findOne({ where: {device_id: device_id} }).then(callback);
//}
//
//module.exports.setUser = function(device_id, email, phone, lat, lng, callback){
//    sequelize.sync().then(function() {
//        return User.create({
//                        device_id: device_id,
//                        email: email,
//                        phone: phone,
//                        lat: lat,
//                        lng: lng
//                        }).then(callback);
//      });
//}
//
//
//module.exports.getComments = function(lat, lng, callback){
//    var distance = 1.000
//    console.log('lat:'+lat);
//    console.log('lng:'+lng);
//    Comment.findAll({ where: {
//                        $and: {
//                            lat: {
//                                $between: [lat - distance, lat + distance]
//                                },
//                            lng: {
//                                $between: [lng - distance, lng + distance]
//                                }
//                            }
//                        }
//                    }).then(callback);
//}