// config/passport.js

// load all the things we need
var FacebookStrategy = require('passport-facebook').Strategy;

// load up the user model
var User       = require('../models/userModel');

// load the auth variables
var configAuth = require('../common/config');

// load the auth mysql
var sql = require('./mysql');

module.exports = function(passport) {

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        console.log('id:'+id);
        console.log('done:'+done);
                    done(err);
//        User.findById(id, function(err, user) {
//            done(err, user);
//        });
    });

    // code for login (use('local-login', new LocalStategy))
    // code for signup (use('local-signup', new LocalStategy))

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.oauth.facebook.clientID,
        clientSecret    : configAuth.oauth.facebook.clientSecret,
        callbackURL     : configAuth.oauth.facebook.callbackURL

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        console.log('profile:'+JSON.stringify(profile));
        console.log('token:'+token);
        // asynchronous
        process.nextTick(function() {

            var query = 'select * from user where social_id = ' + profile.id + ' and social_name=\'facebook\';';
            sql.select(query, function(err, result) {
                if (err) {return done(err)};

                if (result.length > 0) {
                    console.log('result:'+JSON.stringify(result[0]));
                    return done(null, result[0]);
                } else {


                    var insertQuery = 'call signup(\'http://graph.facebook.com/'+profile.id+'/picture?type=large\', \'hyochan\', \''+profile.email+'\', \'19870302\', \'M\', \'Korea\', \''+profile.id+'\', \'facebook\', \''+token+'\');';
                    sql.executeNoneQuery(insertQuery, [], function(err, result) {
                        if (err) {throw done(err)};

                        console.log('insertQuery result:'+JSON.stringify(result[0]));
                        return done(null, result[0]);
                    });
                }
            });
//            // find the user in the database based on their facebook id
//            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {
//
//                // if there is an error, stop everything and return that
//                // ie an error connecting to the database
//                if (err)
//                    return done(err);
//
//                // if the user is found, then log them in
//                if (user) {
//                    return done(null, user); // user found, return that user
//                } else {
//                    // if there is no user found with that facebook id, create them
//                    var newUser            = new User();
//
//                    // set all of the facebook information in our user model
//                    newUser.facebook.id    = profile.id; // set the users facebook id
//                    newUser.facebook.token = token; // we will save the token that facebook provides to the user
//                    newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
//                    newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
//
//                    // save our user to the database
//                    newUser.save(function(err) {
//                        if (err)
//                            throw err;
//
//                        // if successful, return the new user
//                        return done(null, newUser);
//                    });
//                }
//
//            });
        });

    }));

};