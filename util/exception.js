module.exports = function(req, res, errorCode, errorResult) {
    var message = require("../model/messageModel.js");
    console.log(errorCode);
    console.log(errorResult);
    message.errorMsg.code = errorCode;
    message.errorMsg.requestUrl = req.originalUrl;
    message.errorMsg.message = getErrorMessage(errorCode);
    message.errorMsg.desc = errorResult.message == undefined ? '' : errorResult.message ;

    res.send(errorCode, message.errorMsg);

    message.errorMsg.etc = req.body;
    console.log(message.errorMsg);

    return;
};

function getErrorMessage(errorCode) {
    var returnStr = "요청한 대상이 없습니다.";
    switch (errorCode) {
        case 400:
            returnStr = "해당 요청을 수행할 수 없습니다. 요청하신 내용을 다시 한번 확인바랍니다.";
            break;
        case 403:
            returnStr = "권한이 없습니다.";
            break;
        case 404:
            returnStr = "요청하신 대상이 없습니다.";
            break;
        case 4041:
            returnStr = "요청하신 회원 정보가 없습니다.";
            break;
        case 408:
            returnStr = "요청 시간이 초과되었습니다.";
            break;
        case 500:
            returnStr = "서버에 문제가 발생하였습니다. 잠시후에 다시 시도하여 주십시오.";
            break;
        case 5001:
            returnStr = "서버에 문제가 발생하였습니다.";
            break;
        case 5002:
            returnStr = "서버에 문제가 발생하였습니다.";
            break;
        case 5003:
            returnStr = "서버에 문제가 발생하였습니다.";
            break;

    }

    return returnStr;
}