var config = require('../common/config');
var mysql = require('mysql');

var Sequelize = require('sequelize');

exports.sequelize = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
                                                                host: config.mysql.hostName,
                                                                dialect: 'mysql',

                                                                pool: {
                                                                  max: 5,
                                                                  min: 0,
                                                                  idle: 10000
                                                                },

                                                              });


//connection을 사용한 후에, 반드시 connection.release 를 이용해서 pool에 connection을 반납해야 한다.
//특히 에러가 났을때도 connection을 반납해야 하는 것을 잊지 말아야 한다.
//이렇게 connection을 반납하지 않아서 유휴 conneciton이 pool에 남아있지 않는 현상을 connection leak이라고 하는데, 
//connection pool을 사용할 때 상당히 자주 발생하는 장애이기 때문에 반드시 꼼꼼하게 처리하기 바란다. 
//(나중에 leak이 발생하면 찾기도 어렵다.)

var pool = mysql.createPool({
    host     : config.mysql.hostName,
    port     : config.mysql.port,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.database,
    connectionLimit:50, //50개까지 접속가능
    waitForConnections: false,
    multipleStatements : true
});

//추가사항 : 이렇게 해줘도 끊기는 현상이 발생 할 때가 있는데,
//이건 최후의 방법인데 특정 시간마다 연결했다 끊는방법으로 처리
function keepAlive(){
    pool.getConnection(function(err, connection){
        if(err) { return; }
        connection.ping();
        connection.release();
    });
    
    //redis client를 사용중이라면, 아마 Redis연결도 빠르게 끊길겁니다.
    //client.ping();  // 라고 해주면 Redis연결도 유지합니다.
}
setInterval(keepAlive, 60*1000);


/*
사용 예제..!
connection사용 후 반드시 connection.release(); 해야함..!!!
sql.getConnection(function(err, connection) {
  if(err) { console.log(err); res.send(500,"Server Error"); return; }
  
    connection.query("select * from test", [], function(err, results) {
      connection.release();
      if(err) { console.log(err); res.send(500,"de query Error"); return; }
      res.send(results);
    });
});
*/


exports.getConnection = function(callback) {
    pool.getConnection(function(err, connection) {
        callback(err, connection);
    });
};


exports.select = function(queryString, callback) {
  // get a connection from the pool
  pool.getConnection(function(err, connection) {
    if(err) { console.log(err); callback(true, err); return; }
    // make the query
    connection.query(queryString, [], function(err, results) {
      connection.release();
      if(err) { callback(true, err); return; }
      callback(false, results);
    });
  });
};

exports.executeNoneQuery = function(query, params, callback) {
    pool.getConnection(function(err, connection) {
        if(err) { console.log(err); callback(true, err); return; }
        connection.query(query, params, function(err, results) {
            connection.release();
            if(err) { console.log(err); callback(true, err); return; }
            callback(false, results);
        });
    });
}

//exports.executeNoneQuery = function(connection, query, params, callback) {
//    connection.query(query, params, function(err, results) {
//      if(err) {
//        console.log(err); callback(true); throw err;
//      }
//      callback(false, results);
//    });
//}
