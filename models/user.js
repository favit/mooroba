module.exports = function(sequelize, DataTypes) {
    return sequelize.define('User', {
           id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               autoIncrement: true
               },
           social_id: {
                       type: DataTypes.INTEGER,
                       unique: true,
                       validate: {
                           notNull: true
                       }
                   },
           email: {
                   type: DataTypes.STRING(50),
                   unique: true,
                   validate: {
                       isEmail: true
                   }
               },
           birthday: {
                   type: DataTypes.DATEONLY,
                   validate: {
                       notNull: true
                   }
               },
           gender: {
                   type: DataTypes.STRING(1),
                   defaultValue: 'M',
               },
           country: {
                   type: DataTypes.INTEGER
               },
           lat: {
               type: DataTypes.DOUBLE,
               defaultValue: 37.564174
           },
           lng: {
               type: DataTypes.DOUBLE,
               defaultValue: 126.997576
           },
           points: {
               type: DataTypes.INTEGER,
               defaultValue: 0
           },
           grade: {
               type: DataTypes.STRING(2),
               defaultValue: 'A'
           },
           start_date: {
               type: DataTypes.DATEONLY
           },
           end_date: {
               type: DataTypes.DATEONLY
           }},
           {
             timestamps: true,
             createdAt: 'create_date',
             updatedAt: 'update_date',
       
             tableName: 'user'
           });
}