module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Poll', {
           id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               autoIncrement: true
               },
           user_id: {
                       type: DataTypes.INTEGER,
                       validate: {
                           notNull: true
                       }
                   },
           bg_img: {
                   type: DataTypes.STRING(100),
               },
           content: {
                   type: DataTypes.TEXT('tiny'),
                   validate: {
                       notNull: true
                   }
               },
           option: {
                   type: DataTypes.STRING(5),
                   validate: {
                       notNull: true
                   }
               },
           status: {
                   type: DataTypes.STRING(2),
                   defaultValue: 'P'
               }
           },
           {
             timestamps: true,
             createdAt: 'create_date',
             updatedAt: 'update_date',
       
             tableName: 'poll'
           });
}