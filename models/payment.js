module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Payment', {
            id: {
                   type: DataTypes.INTEGER,
                   primaryKey: true,
                   autoIncrement: true
               },
           user_id: {
                   type: DataTypes.INTEGER,
                   validate: {
                       notNull: true
                   }
               },
           amount: {
                   type: DataTypes.INTEGER,
                   defaultValue: 0
               },
           method: {
                   type: DataTypes.STRING(15),
                   defaultValue: 'naver'
               }
           },
           {
             timestamps: true,
             createdAt: 'create_date',
             updatedAt: 'update_date',

             tableName: 'payment'
           });
}
