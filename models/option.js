module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Option', {
           id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               autoIncrement: true
               },
           poll_id: {
                   type: DataTypes.INTEGER,
                   primaryKey: true,
                   validate: {
                       notNull: true
                   }
               },
           text: {
                   type: DataTypes.STRING
               },
           img: {
                   type: DataTypes.STRING
               }
           },
           {
             timestamps: false,
       
             tableName: 'option'
           });
}