module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Point', {
        user_id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               validate: {
                   notNull: true
               }
           },
       payment_id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               validate: {
                   notNull: true
               }
           },
       point: {
               type: DataTypes.INTEGER,
               defaultValue: 0
           },
       status: {
               type: DataTypes.STRING(10),
               defaultValue: 'BUY'
           }
       method: {
               type: DataTypes.STRING(15),
               defaultValue: 'naver'
           }
       },
       {
         timestamps: true,
         createdAt: 'create_date',
         updatedAt: 'update_date',
   
         tableName: 'point'
       });
}