exports.User = function(){
    this.id = 1;
    this.pic = null;
    this.name = 'Guest';
    this.email = null;
    this.lat = null;
    this.lng = null;
    this.points = 0;
    this.grade = 'A';

    return this;
};