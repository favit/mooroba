module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Selection', {
           user_id: {
                   type: DataTypes.INTEGER,
                   primaryKey: true,
                   validate: {
                       notNull: true
                   }
               },
           poll_id: {
                   type: DataTypes.INTEGER,
                   primaryKey: true,
                   validate: {
                       notNull: true
                   }
               },
           option_id: {
                   type: DataTypes.INTEGER,
                   validate: {
                       notNull: true
                   }
               }
           },
           {
             timestamps: true,
             createdAt: 'create_date',
             updatedAt: false,
       
             tableName: 'selection'
           });
}