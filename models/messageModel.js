module.exports = {
//대화내용 담아서 보낼 데이터(변수명 : 들어갈 데이터)
	'messageModel'    : {
    'requesterId'     : '',
    'requestUrl'      : '',
    'requestMessage'  : '',
    'responseMessage' : '',
    'errorCode'       : '',
    'result'          : ''
    },

    'errorMsg' : {
    'code'       : '',
    'requestUrl' : '',
    'message'    : '',
    'desc'       : '',
    'etc'        : ''
    },

    'resultMsg' : {
    'result' : 'SUCCESS'
    }
};
